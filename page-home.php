<?php 
//Template Name: Home Page
?>

<?php get_header(); ?>
<div class="banner">
       <div>
           <div class="innerbox">
               <div><h1><?php
                    
                    the_field('titulo_chamativo'); 
                ?>
                </h1></div>
                <div class ="line"></div>
                <div><p><?php the_field('sub-titulo_chamativo') ?></p></div>
            </div>
       </div>
    </div>
    <div class="block2desc">
        <div>
            <div><h1>Uma Mistura de</h1></div>
            <div class="amorperfeição">
                <div>
                    <img src="<?php the_field('primeira_imagem')  ?>">
                    <div class="desctxt"><p>amor</p></div>
                </div>
                <div>
                    <img src="<?php the_field('segunda_imagem')  ?> ">
                    <div class="desctxt"><p>perfeição</p>
                </div>
            </div>
            </div>

            <div class="txt"><div><p><?php the_field('paragrafo_de_chamatriz') ?></p></div></div>
        </div>
        <span id="sobre"></span>
    </div>
    <div class = "division"></div>
    <div>
        <span id="produtos"></span>
        <div class="block3">
            <div>
                <div><div class="circle1"><div class="circleborder1"><div class="circlecenter1"></div></div></div></div>
                <h1><?php $produto = get_page_by_title('Produtos');
                    the_field('sabor_cafe_um', $produto); ?> </h1>
                <p><?php $produto = get_page_by_title('Produtos');
                    the_field('paragrafo_cafe_um', $produto); ?>
                </p>
            </div>
            <div>
                <div><div class="circle2"><div class="circleborder2"><div class="circlecenter2"></div></div></div></div>
                <h1><?php $produto = get_page_by_title('Produtos');
                    the_field('sabor_cafe_dois', $produto); ?> </h1>
                <p><?php $produto = get_page_by_title('Produtos');
                    the_field('paragrafo_cafe_dois', $produto); ?>
                </p>
            </div>
            <div>
                <div><div class="circle3"><div class="circleborder3"><div class="circlecenter3"></div></div></div></div>
                <h1><?php $produto = get_page_by_title('Produtos');
                    the_field('sabor_cafe_tres', $produto); ?> </h1>
                <p><?php $produto = get_page_by_title('Produtos');
                    the_field('paragrafo_cafe_tres', $produto); ?>
                </p>
            </div>
            <span class="saibamais">
                <div class="invert"><a href="">Saiba Mais</a></div>
            </span>
        </div>
    </div>
    <div class = "division"></div>
    <span id="portfólio"></span>
    <div class="lastblock">
        <div class="item">
            <img src="<?php the_field('cafeteria_a') ?>">
            <div class="itemtxt" id="itemtxt">
                <div><h1><?php the_field('bairro_um') ?></h1></div>
                <div><p><?php the_field('paragrafo_loja_a') ?></p></div>
                <div class="vermapabox"><a class="vermapa" href="https://www.google.com/maps/place/Origamid/@-22.9567307,-43.1920258,14.36z/data=!4m5!3m4!1s0x9be1553784b685:0x630552de6ab90fca!8m2!3d-22.9572162!4d-43.1761896">VER MAPA</a></div>
            </div>
        </div>
        <div class="item">
            <img src="<?php the_field('cafeteria_b') ?>">
            <div class="itemtxt" id="itemtxt">
                <div><h1><?php the_field('bairro_dois') ?></h1></div>
                <div><p><?php the_field('paragrafo_loja_b') ?></p></div>
                <div class="vermapabox"><a class="vermapa" href="">VER MAPA</a></div>
            </div>
        </div>
        <div class="item">
            <img src="<?php the_field('cafeteria_c') ?>">
            <div class="itemtxt" id="itemtxt">
                <div><h1><?php the_field('bairro_tres') ?></h1></div>
                <div><p><?php the_field('paragrafo_loja_c') ?></p></div>
                <div class="vermapabox"><a class="vermapa" href="">VER MAPA</a></div>
            </div>
        </div>
    </div>
    <span id="contato"></span>
    <?php get_footer() ?>